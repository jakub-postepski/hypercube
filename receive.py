#!/usr/bin/env python3
import pika, sys, os, time
import pika, threading

from rpi_ws281x import PixelStrip, Color
import argparse
import json
import time

global command
command = {"button": "bPulse", "color_rgb": [255, 0, 0]}

def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        global command
        command = json.loads(body)
        print(command)
        print(" [x] Received %r" % body)

    channel.basic_consume(callback, "hello", True)

    print(' [*] Waiting for messages. To exit press CTRL+C')

    channel.start_consuming()    
    # main_leds()


# LED strip configuration:
LED_COUNT = 375       # Number of LED pixels.
LED_PIN = 18          # GPIO pin connected to the pixels (18 uses PWM!).
# LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10          # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255  # Set to 0 for darkest and 255 for brightest
LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


CUBE = [
    ('a0', 'a3', 0, 14),
    ('a3', 'b3', 17, 20),
    ('b3', 'b0', 22, 31),
    ('b0', 'b4', 34, 42), 
    ('a3', 'a7', 43, 57), 
    ('a3', 'a2', 59, 74), 
    ('a2', 'b2', 76, 81), 
    ('b2', 'b3', 84, 92), 
    ('b3', 'b7', 95, 103), 
    ('a2', 'a6', 105, 119), 
    ('a2', 'a1', 120, 135), 
    ('a1', 'b1', 137, 141), 
    ('b1', 'b2', 143, 151), 
    ('b2', 'b6', 153, 161), 
    ('a1', 'a5', 162, 176), 
    ('a1', 'a0', 177, 193), 
    ('a0', 'b0', 196, 200), 
    ('b0', 'b1', 202, 209), 
    ('b1', 'b5', 212, 219), 
    ('a0', 'a4', 221, 236), 
    ('a4', 'a7', 237, 253), 
    ('b0', 'b7', 255, 264), 
    ('b7', 'a7', 267, 271), 
    ('a7', 'a6', 273, 287), 
    ('b7', 'b6', 288, 294), 
    ('b6', 'a6', 296, 300), 
    ('a6', 'a5', 307, 323), 
    ('b6', 'b5', 324, 330), 
    ('b5', 'a5', 332, 336), 
    ('a5', 'a4', 338, 353), 
    ('a4', 'b4', 355, 359), 
    ('b4', 'b2', 361, 368)
]

# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=30):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms / 1000.0)

def oneLed(strip, color, pos=0, bck_color=Color(0, 0, 0)):
    for i in range(strip.numPixels()):
        if i == pos:
            strip.setPixelColor(i, color)
        else:
            strip.setPixelColor(i, bck_color)    
    strip.show()



def theaterChase(strip, color, wait_ms=50, iterations=10):
    """Movie theater light style chaser animation."""
    for j in range(iterations):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, color)
            strip.show()
            time.sleep(wait_ms / 1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, 0)


def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


def rainbow(strip, wait_ms=5, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256 * iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i + j) & 255))
        strip.show()
        yield
        

def oneEdge(strip, begin_idx=0, end_idx=10, color=Color(255, 0, 0), bck_color=Color(0,255,255)):
    direction = 1 if begin_idx < end_idx else -1
    for pos in range(begin_idx, end_idx, direction):
        for i in range(strip.numPixels()):
            if i == pos or i - 1 == pos or i + 1 == pos:
                strip.setPixelColor(i, color)
            else:
                strip.setPixelColor(i, bck_color)    
        strip.show()
        yield


def pathGen(strip, path=['a1', 'a2', 'b3'], r=255, g=0, b=0):
    color = Color(r, g, b)
    bck_color = Color(255-r, 255-g, 255-b)
    path_idxs = []

    for i in range(len(path)-1):
        
        v1 = path[i]
        v2 = path[i+1]

        if v2[0] == 'x':
            time.sleep(int(v1[1]))
            break
        if v1[0] == 'x':
            break
        
        #searching edge
        for e in CUBE:
            if e[0] == v1 and e[1] == v2:
                path_idxs += [z for z in range(e[2], e[3]+1)]
                break
            if e[0] == v2 and e[1] == v1:
                path_idxs += [z for z in range(e[3], e[2]-1, -1)]
                break

    # for pos in range(len(path_idxs)):
    #     for i in range(strip.numPixels()):
    #         if i == path_idxs[pos] or i == path_idxs[min(pos+1, strip.numPixels())] or i == path_idxs[max(pos-1, 0)]:
    #             strip.setPixelColor(i, color)
    #         else:
    #             strip.setPixelColor(i, bck_color)
    #     strip.show()
    #     yield


    active_list = []
    for pos in path_idxs:
        active_list.append(pos)
        for i in range(strip.numPixels()):
            if i in active_list:
                strip.setPixelColor(i, color)
            else:
                strip.setPixelColor(i, Color(0, 0, 0))
        strip.show()
        yield



def rainbowCycle(strip, wait_ms=20, iterations=1):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256 * iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel(
                (int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()
        time.sleep(wait_ms / 1000.0)


def theaterChaseRainbow(strip, wait_ms=50):
    """Rainbow movie theater light style chaser animation."""
    for j in range(256):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, wheel((i + j) % 255))
            strip.show()
            time.sleep(wait_ms / 1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i + q, 0)


# Main program logic follows:
def main_leds():
    global command
    # Create NeoPixel object with appropriate configuration.
    strip = PixelStrip(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()
    try:
        print('Color wipe animations.') 
        previous_state = command["button"]
        colorWipe(strip, Color(0, 255, 255))  # Red wipe
        one_led_idx = None
        rainbow_gen = rainbow(strip)
        temp_gen = None
        odd_loop = True 
        while True:
            try:
                r = int(command["color_rgb"][0])
                g = int(command["color_rgb"][1])
                b = int(command["color_rgb"][2])

                color = Color(r, g, b)

                if command["button"] == "bPath1":
                    command["paths_list"] = ['b1', 'b2', 'b3', 'b7']

                if command["button"] == "bPath2":
                    command["paths_list"] = ['a0', 'a4', 'a7', 'a6']

                if command["button"] == "bPath3":
                    command["paths_list"] = ['a0', 'b0', 'b1', 'b5']

                
                if command["button"] == "bPulse":
                    if previous_state != command["button"]:
                        rainbow_gen = rainbow(strip)
                    try:
                        next(rainbow_gen)
                    except StopIteration:
                        rainbow_gen = rainbow(strip)
                elif command["button"] == "bConst":
                    one_led_idx = None
                    colorWipe(strip, color, 20)
                elif command["button"] == "bOff":
                    one_led_idx = None
                    colorWipe(strip, Color(0, 0 ,0), 20)
                elif "bPath" in command["button"] or "save" in command["button"]:
                    if previous_state != command["button"] or temp_gen == None:
                        temp_gen = pathGen(strip, command["paths_list"], r, g, b)
                    try:
                        next(temp_gen)
                    except StopIteration:
                        temp_gen = pathGen(strip, command["paths_list"], r, g, b)
                elif command["button"] == "bOne":
                    one_led_idx = 0
                elif command["button"] == "bDemonstrate":
                    if command["button"] != previous_state:
                        colorWipe(strip, Color(0, 0, 0))
                        odd_loop = True
                    if odd_loop:
                        colorWipe(strip, Color(r, g, b))
                    else:
                        colorWipe(strip, Color(0, 0, 0))
                    odd_loop = not odd_loop


                if not one_led_idx is None:
                    if command["button"] == "bFwd":
                        if one_led_idx + 1 < LED_COUNT:
                            one_led_idx += 1
                    elif command["button"] == "bBck":
                        if one_led_idx - 1 >= 0:
                            one_led_idx -= 1
                    elif command["button"] == "b10Fwd":
                        if one_led_idx + 10 < LED_COUNT:
                            one_led_idx += 10
                    elif command["button"] == "b10Bck":
                        if one_led_idx - 10 >= 0:
                            one_led_idx -= 10
                    command["button"] = "bAfterOne"
                    print(f"one_led_idx{one_led_idx}")
                    oneLed(strip, color, one_led_idx)
                
                if command["button"] == "bPulse" or "bPath" in command["button"]:
                    wait_ms = 40
                    time.sleep(wait_ms / 1000.0)
                else:
                    time.sleep(0.2)

                previous_state = command["button"]
            except Exception as e:
                print(str(e))
            

    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0, 0, 0), 10)




from threading import Thread


if __name__ == '__main__':
    try:
        t1 = Thread(target=main)
        t2 = Thread(target=main_leds)
        t1.start()
        t2.start()
        # main()
        # main_leds()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            t1.join()
            t2.join()
            sys.exit(0)
        except SystemExit:
            os._exit(0)