from flask import Flask, render_template, send_from_directory, request, jsonify
import time

import pika
import json

import sqlite3

global LED_PATHS
LED_PATHS = []


def create_app():
  app = Flask(__name__)

  return app

app = create_app()


@app.route('/')
def index():
    buttons = [
        {"id": "bPulse", "type": "primary", "text": "Pulsowanie"},
        {"id": "bDemonstrate", "type": "primary", "text": "Pokaz możliwości"},
        {"id": "bConst", "type": "primary", "text": "Stałe światło"},
        {"id": "bOff", "type": "primary", "text": "Wyłączanie"},
        {"id": "bPath", "type": "primary", "text": "Ścieżka"},
        {"id": "bPath1", "type": "primary", "text": "Ścieżka w środku"},
        {"id": "bPath2", "type": "primary", "text": "Ścieżka na zewnątrz"},
        {"id": "bPath3", "type": "primary", "text": "Ścieżka pomiędzy"},
        {"id": "bOne", "type": "primary", "text": "Jedna dioda"},
        {"id": "bFwd", "type": "secondary", "text": "1 w przód"},
        {"id": "bBck", "type": "secondary", "text": "1 w tył"},
        {"id": "b10Fwd", "type": "secondary", "text": "10 w przód"},
        {"id": "b10Bck", "type": "secondary", "text": "10 w tył"},
    ]
    global LED_PATHS
    LED_PATHS = read_paths()
    print(LED_PATHS)
    return render_template('index.html', buttons = buttons, paths=LED_PATHS)

@app.route('/command', methods=['GET', 'POST'])
def command():
    content = request.get_json()
    color_rgb = None
    paths_list = None
    try:
        color_rgb = content.get("color", [128, 0, 0]).split(";")
        paths_list = []
        paths = content.get("path", "A1B1")
        if content.get("id", "")  == "bPath1":
            paths = "B3B7"

        if content.get("id", "")  == "bPath2":
            paths = "A3A7"

        if content.get("id", "")  == "bPath3":
            paths = "A4B2"

        if len(paths) < 4 or len(paths) % 2: raise Exception("Wrong path args count")
        odd = True
        cube = None
        for x in paths:
            if odd:
                if not x in ['A', 'B', 'a', 'b', 'X', 'x']: raise Exception("Wrong cube")
                if x == 'A':
                    x = 'a'
                if x == 'B':
                    x = 'b'
                if x == 'X':
                    x = 'x'
                cube = x
            else:
                vertex = int(x)
                if vertex < 0 or vertex > 8: raise Exception("Wrong vertex number")
                paths_list.append(f"{cube}{vertex}")
                cube = None
            odd = not odd
    except Exception as e:
        return render_template('status.html', error = str(e), status = "OK")
    print(color_rgb)
    print(paths_list)
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()


    channel.queue_declare(queue='hello')

    channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=json.dumps({"color_rgb": color_rgb, "paths_list": paths_list, "button": content.get("button", "off")}))
    print(" [x] Sent 'Hello World!'")


    connection.close()

    d = {"status": "OK", "error": ""}
    return jsonify(d)


@app.route('/static/<path:path>')
def static_files(path):
    return send_from_directory('static', path)

@app.route('/status')
def status():
    d = {"status": "OK", "error": ""}
    return jsonify(d)


# Endpoint to read JSON file and convert it to a Python dictionary
# @app.route('/read_paths', methods=['GET'])
def read_paths():
    file_path = 'data.json'  # Path to your JSON file
    global LED_PATHS
    LED_PATHS = []
    try:
        with open(file_path, 'r') as file:
            LED_PATHS = json.load(file)
    except:
        LED_PATHS = [{"id": x, "value": "B3B7", "color": "0;255;255"} for x in range(10)]
    return LED_PATHS

# Endpoint to save a Python dictionary as a JSON file
@app.route('/save_path', methods=['POST'])
def save_paths():
    file_path = 'data.json'  # Path to save the JSON file

    content = request.get_json()
    print(content)
    global LED_PATHS
    LED_PATHS[int(content['id'])] = content
    print(content)
    with open(file_path, 'w') as file:
        json.dump(LED_PATHS, file)


    try:
        color_rgb = content.get("color", [128, 0, 0]).split(";")
        paths_list = []
        paths = content.get("value", "A1B1")
        if len(paths) < 4 or len(paths) % 2: raise Exception("Wrong path args count")
        odd = True
        cube = None
        for x in paths:
            if odd:
                if not x in ['A', 'B', 'a', 'b']: raise Exception("Wrong cube")
                if x == 'A':
                    x = 'a'
                if x == 'B':
                    x = 'b'
                if x == 'X':
                    x = 'x'
                cube = x
            else:
                vertex = int(x)
                if vertex < 0 or vertex > 8: raise Exception("Wrong vertex number")
                paths_list.append(f"{cube}{vertex}")
                cube = None
            odd = not odd
    except Exception as e:
        return render_template('status.html', error = str(e), status = "OK")
    print(color_rgb)
    print(paths_list)
    connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    channel = connection.channel()


    channel.queue_declare(queue='hello')

    channel.basic_publish(exchange='',
                      routing_key='hello',
                      body=json.dumps({"color_rgb": color_rgb, "paths_list": paths_list, "button": "bPath"}))
    print(" [x] Sent 'Hello World!'")


    connection.close()

    d = {"status": "OK", "error": ""}
    return jsonify(d)
    return jsonify({'message': 'Data saved successfully'})



if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
